const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema(
	{
		firstName: {
			type: String,
			required: [true, 'First name is required!']
		},

		lastName: {
			type: String,
			required: [true, 'Last name is required!']
		},

		email: {
			type: String,
			required: [true, 'Email is required!'] 
		},

		password: {
			type: String
		},

		isAdmin: {
			type: Boolean,
			default: false
		},

		loginType: {
			type: String,
			required: [true, 'loginType is required']
		},

		mobileNumber: {
			type: String
		},

		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is required!']
				},

				enrolledOn: {
					type: Date,
					default: new Date()
				},

				status: {
					type: String,
					default: 'enrolled'
				}
			}
		]
	}
);

module.exports = mongoose.model('User', UserSchema);