const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');
const auth = require('../auth');

router.post('/', (req, res) => {
	UserController.register(req.body)
	.then(result => {
		res.send(result)
	})
});

router.post('/login', (req,res) => {
	UserController.login(req.body)
	.then(result => {
		res.send(result)
	})
});

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	UserController.getDetails({userId: user.id})
	.then(result => {
		res.send(result)
	});
});

router.get('/:userId', (req, res) => {

	UserController.getDetails({userId: req.params.userId})
	.then(result => res.send(result))
})

router.get('/', (req, res) => {
	UserController.getAll()
	.then(users => res.send(users))
})

router.post('/verifyGoogleIdToken', (req, res) => {
	UserController.verifyGoogleTokenId(req.body.tokenId).then(result => res.send(result));
});

module.exports = router;