const express = require('express');
const router = express.Router();
const auth = require('../auth');
const CourseController = require('../controllers/CourseController');

router.get('/', (req, res) => {
	CourseController.getAll()
	.then(courses => res.send(courses))
});

router.get('/:id', (req, res) => {
	CourseController.getOne({id: req.params.id})
	.then(course => res.send(course))
})

router.post('/', auth.verify,  (req, res) => {

	const params = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	CourseController.addCourse(params)
	.then(result => res.send(result))
});

router.put('/:id', auth.verify, (req, res) => {
	
	const params = {
		courseId: req.params.id,
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	CourseController.updateCourse(params)
	.then(result => res.send(result))
});

router.delete('/:id', auth.verify, (req, res) => {
	
	const params = {
		courseId: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	CourseController.deleteCourse(params)
	.then(result => res.send(result))
});

router.put('/:courseId/enrollments', auth.verify, (req, res) => {

	const params = {
		courseId: req.params.courseId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	CourseController.enrollUser(params)
	.then(result => res.send(result))
});

module.exports = router;