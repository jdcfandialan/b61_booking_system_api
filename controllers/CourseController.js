const Course = require('../models/Course');
const User = require('../models/User');
const request = require('request');
const clientId = '68746645342-at8lh921jbdjt5jqcbco58p719efkr4j.apps.googleusercontent.com';

module.exports.getAll = () => {
	
	return Course.find({
		isActive: true
	}).then(courses => courses)
}

module.exports.getOne = (params) => {

	return Course.findById(params.id)
	.then((course, error) => {
		if(error) return false
		return course
	})
}

module.exports.addCourse = (params) => {
	
	if(params.isAdmin){
		const course = new Course({
			name: params.name,
			description: params.description,
			price: params.price,

		});

		return course.save()
		.then((course, error) => {
			return (error) ? false : true
		});
	}

	else return false
}

module.exports.updateCourse = (params) => {
	
	if(params.isAdmin){
		return Course.findById(params.courseId)
		.then((course, error) => {
			if(error) return false

			else{
				course.name = params.name;
				course.description = params.description;
				course.price = params.price;

				return course.save()
				.then((updatedCourse, error) => {
					return (error) ? false : true
				});
			}
		});
	}

	else return false
}

module.exports.deleteCourse = (params) => {

	if(params.isAdmin){
		return Course.findById(params.courseId)
		.then((course, error) => {
			if(error) return false

			else{
				course.isActive = false;

				return course.save()
				.then((updatedCourse, error) => {
					return (error) ? false : true
				});
			}
		});
	}

	else return false
}

module.exports.enrollUser = (params) => {

	if(!(params.isAdmin)){
		return Course.findById(params.courseId)
		.then((course, error) => {
			if(error) return false

			else{
				if(course.isActive){
					let filterCourse = [];

					for(let i = 0;i < course.enrollees.length;i++){
						if(course.enrollees[i].userId === params.userId)
							filterCourse.push(course.enrollees[i])
					}

					if(filterCourse.length > 0) return false

					else{

						course.enrollees.push({
							userId: params.userId
						})

						return course.save()
						.then((updatedCourse, error) => {
							if(error) return false
							
							else{
								return User.findById(params.userId)
								.then((user, error) => {
									if(error) return false

									else{
										let filterUser = [];
										for(let i = 0;i < user.enrollments.length;i++){
											if(user.enrollments[i].courseId === params.courseId)
												filterUser.push(user.enrollments[i]);
										}

										if(filterUser.length > 0) return false
										
										else{
											user.enrollments.push({
												courseId: params.courseId
											})

											return user.save()
											.then((updatedUser, error) => {
												if(error) return false

												else{

													const shortCode = '21585618';
													const accessToken = '2soBh0BsAWnyQmnfn5yde8aETmkgf4wlZCN2xf4_e1U';
													const clientCorrelator = '123456';
													const mobileNumber = updatedUser.mobileNumber;
													const message = `Successfully enrolled in ${updatedCourse.name}!!`; 

													const options = {
														method: 'POST',
														url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
														qs: {'access_token': accessToken},
														headers: {
															'Content-Type': 'application/json'
														},
														body: {
															'outboundSMSMessageRequest' : {
																'clientCorrelator': clientCorrelator,
																'senderAddress': shortCode,
																'outboundSMSTextMessage': {'message': message},
																'address': mobileNumber
															}
														}, json: true
													}

													request(options, (error, response, body) => {
														if(error) throw new Error(error)

														console.log(body)
													})

													return true
												}
											})
										}
									}
								})	
							}
						})
					}
				}

				else return false
			}
		})
	}

	else return false
}