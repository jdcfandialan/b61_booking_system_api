const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const {OAuth2Client} = require('google-auth-library');
const request = require('request');
const clientId = '68746645342-at8lh921jbdjt5jqcbco58p719efkr4j.apps.googleusercontent.com';

module.exports.register = (params) => {
	const user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'default',
		mobileNumber: params.mobileNumber
	});

	return user.save()
	.then((user, error) => {
		if(error) return false

		else{
			const shortCode = '21585618';
			const accessToken = '2soBh0BsAWnyQmnfn5yde8aETmkgf4wlZCN2xf4_e1U';
			const clientCorrelator = '123456';
			const mobileNumber = user.mobileNumber;
			const message = 'You have registered successfully!!';

			const options = {
				method: 'POST',
				url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
				qs: {'access_token': accessToken},
				headers: {
					'Content-Type': 'application/json'
				},
				body: {
					'outboundSMSMessageRequest' : {
						'clientCorrelator': clientCorrelator,
						'senderAddress': shortCode,
						'outboundSMSTextMessage': {'message': message},
						'address': mobileNumber
					}
				}, json: true
			}

			request(options, (error, response, body) => {
				if(error) throw new Error(error)

				console.log(body)
			})

			return true
		}
	});
}

module.exports.login = (params) => {
	
	return User.findOne({email: params.email})
	.then(user => {
		if(user === null){
			return false
		}
		
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password);

		if(isPasswordMatched){
			return {
				accessToken: auth.createAccessToken(user.toObject())
			}
		}

		else return false
	});
}

module.exports.getAll = () => {

	return User.find({})
	.then(users => users)
}

module.exports.getDetails = (params) => {

	return User.findById(params.userId)
	.then((user, error) => {
		if(error) return false

		user.password = undefined
		return user
	})
}

module.exports.verifyGoogleTokenId = async tokenId => {
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({idToken: tokenId , audience: clientId});

	if(data.payload.email_verified){
		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null){
			if(user.loginType === 'google'){
				return {accessToken: auth.createAccessToken(user.toObject())}
			}

			else{
				return {error: 'login-type-error'}
			}
		}

		else{
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			});

			return newUser.save().then((user, error) => {
				//console.log(user);
				return (error) ? false : {accessToken: auth.createAccessToken(user.toObject())}
			});
		}
	}

	else{
		return {error: 'google-auth-error'}
	}
}