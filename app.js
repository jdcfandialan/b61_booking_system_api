const express = require('express');
const mongoose = require('mongoose');
const env = require('dotenv');
const cors = require('cors');

const db = mongoose.connection;
const Schema = mongoose.Schema;

const app = express();

env.config();

const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

mongoose.connect(process.env.DB_MONGO, {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

db.on('error',() => {
	console.error(`'Error on database ${process.env.DB_MONGO} connection!'`)
});

db.once('open',() => {
	console.log(`connected to database ${process.env.DB_MONGO}`)
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const userRoutes = require('./routes/UserRouter');
app.use('/api/users', cors(corsOptions), userRoutes);

const courseRoutes = require('./routes/CourseRouter');
app.use('/api/courses', cors(corsOptions), courseRoutes);

app.listen(process.env.PORT, ()=>{
	console.log(`Now listening on PORT ${process.env.PORT}`);
});